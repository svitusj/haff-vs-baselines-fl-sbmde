# README - Empowering the Human as the Fitness Function in Search-Based Model-Driven Engineering #

The following materials are included:

* CSV files used as input to report the results and the statistical analysis.
* R-scripts for the additional analysis based on self-rated familiarity (included in the discussion).
* CSV files used as input to report the additional analysis of the discussion (based on self-rated familiarity).
* A screenshot the modeling tool while HaFF is used. The right part of the figure shows the HaFF panel, which includes the form to assess the model fragments manually. For each iteration, the tool will ask the engineer to evaluate each of the 10 model fragments before moving to the next iteration. The form includes a button to visualize each of the model fragments and a select element where the engineer can pick a value from 7 to 1 (being 7 the model fragment that best realizes the feature being located). Once a score has been assigned to each model fragment, the engineer can press the "next iteration" button, so the process can continue.
